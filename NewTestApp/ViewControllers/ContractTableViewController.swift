//
//  MyTableViewController.swift
//  NewTestApp
//
//  Created by macos on 6/28/18.
//  Copyright © 2018 macos. All rights reserved.
//

import UIKit

class ContractTableViewController: UIViewController, UITableViewDataSource, UITableViewDelegate {
    
    // MARK: Dependencies
    private let contractsManager: IContractsManager = ContractsManager.shared
    
    // MARK: IBOutlets
    @IBOutlet weak var tableView: UITableView!
    
    // MARK: Properties
    fileprivate var contracts = [ContractsData]()
    private var isLoading: Bool = true
    private var isTryCell: Bool = false
    
    // MARK: IBActions
    @IBAction func pressTryButton(_ sender: Any) {
        
        loadContracts()
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        isLoading = true
        isTryCell = false
        tableView.reloadData()
        
        let nib = UINib(nibName: "ContractTableViewCell", bundle: Bundle.main)
        tableView.register(nib, forCellReuseIdentifier: "contract cell")
        
        loadContracts()
    }
    
    // MARK: Private methods
    private func loadContracts() {
        
        contractsManager.getContracts { [weak self](contracts, error) in
            if let error = error {
                print("error: \(error)")
                self?.showTryCell()
            }
            
            if var contracts = contracts {
                contracts.sort { ($1.type, $0.sum) > ($0.type, $1.sum)}
                self?.showContracts(contracts)
            } else {
                self?.showTryCell()
            }
        }
    }
    
    private func showContracts(_ contracts: [ContractsData]) {
        
        self.contracts = contracts
        isLoading = false
        tableView.reloadData()
    }
    
    private func showTryCell() {
    
        isLoading = true
        isTryCell = true
        tableView.reloadData()
    }
    
    
    // MARK: UITableViewDataSource
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if isLoading {
            return contracts.count + 1
        } else {
            return contracts.count
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if isLoading {
            let cell = tableView.dequeueReusableCell(withIdentifier: "LoadingCell") as! LoadTableViewCell
            cell.map(showTryButton: isTryCell)
            return cell
        } else {
            let cell = tableView.dequeueReusableCell(withIdentifier: "contract cell") as! ContractTableViewCell
            let model = contracts[indexPath.row]
            cell.map(model: model)
            return cell
        }
    }
}
