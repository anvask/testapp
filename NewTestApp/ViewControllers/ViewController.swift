//
//  ViewController.swift
//  NewTestApp
//
//  Created by macos on 6/17/18.
//  Copyright © 2018 macos. All rights reserved.
//

import UIKit

class ViewController: UIViewController, UITextFieldDelegate {
    
    // MARK: Dependencies
    private var authManager: IAuthorizationManager = AuthManager.shared
    
    // MARK: Constants
    private let kAvailableCharacters = CharacterSet(charactersIn: "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890").inverted
    private let kEnabledColor = UIColor(red: 0, green: 0.5, blue: 1, alpha: 1)
    private let kDisabledColor = UIColor.lightGray
    private let kSuccessCode = 200
    private let kForbiddenCode = 403
    
    // MARK: IBOutlets
    @IBOutlet weak var loginTextField: UITextField!
    @IBOutlet weak var passTextField: UITextField!
    @IBOutlet weak var loginButton: UIButton!
    @IBOutlet weak var activityIndicator: UIActivityIndicatorView!
    @IBOutlet weak var blockingView: UIView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        activityIndicator.isHidden = true
        blockingView.isHidden = true
        loginTextField.delegate = self
        validateLoginButton()
        
    }
    
    // MARK: IBActions
    @IBAction func loginButtonPressed(_ sender: Any) {
        startStopActivityIndicator()
        blockingView.isHidden = false
        
        authManager.authorization(login: loginTextField.text ?? "", password: passTextField.text ?? "") { [weak self](success, error) in
            self?.startStopActivityIndicator()
            self?.blockingView.isHidden = true
            if success {
                self?.showContractList()
            }
            else {
                self?.handleError(error: error)
            }
        }
    }
    
    // MARK: Private methods
    private func showContractList() {        
        if let viewController = storyboard?.instantiateViewController(withIdentifier: "ContractTableViewController") {
            navigationController?.pushViewController(viewController, animated: true)
        }
    }
    
    private func handleError(error: Error?) {
        blockingView.isHidden = true
        let wbError = error as! WebServiceError
        switch wbError {
        case WebServiceError.failedStatusCode:
            showAlert(title: "Авторизация", message: "Неправильный логин или пароль")
        case WebServiceError.noToken:
            showAlert(title: "Авторизация", message: "Внутренняя ошибка сервиса")
        case WebServiceError.invalidResponse:
            showAlert(title: "Авторизация", message: "Неверный ответ сервиса")
        default:
            showAlert(title: "Авторизация", message: "Неизвестная ошибка сервера")
        }        
    }
    
    private func showAlert(title: String, message: String) {
        
        let alert = UIAlertController(title: title, message: message, preferredStyle: .alert)
        let actionOk = UIAlertAction(title: "OK", style: .default, handler: nil)
        alert.addAction(actionOk)
        present(alert, animated: true, completion: nil)
    }
    
    private func startStopActivityIndicator() {
        if activityIndicator.isAnimating == true {
            activityIndicator.stopAnimating()
            activityIndicator.isHidden = true
        } else {
            activityIndicator.startAnimating()
            activityIndicator.isHidden = false
        }
    }
    
    @IBAction func textFieldTextChanged(_ sender: Any) {
        validateLoginButton()
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        var text = textField.text ?? ""
        guard let swiftRange = Range(range, in: text) else {
            return false
        }
        text = text.replacingCharacters(in: swiftRange, with: string)
        text = text.components(separatedBy: kAvailableCharacters).joined()
        textField.text = text
        
        validateLoginButton()
        
        return false
    }
    
    private func validateLoginButton() {
        let login = loginTextField.text ?? ""
        let pass = passTextField.text ?? ""
        let enabled = !login.isEmpty && !pass.isEmpty
        
        loginButton.isEnabled = enabled
        loginButton.backgroundColor = enabled ? kEnabledColor : kDisabledColor
    }
}
