//
//  RoundedItems.swift
//  NewTestApp
//
//  Created by macos on 7/1/18.
//  Copyright © 2018 macos. All rights reserved.
//

import UIKit

class RoundedButton: UIButton {
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        layer.cornerRadius = bounds.height / 2
        layer.borderColor = UIColor.red.cgColor
        layer.borderWidth = 2
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        layer.cornerRadius = bounds.height / 2
        layer.borderColor = UIColor.red.cgColor
        layer.borderWidth = 2
    }
}

class RoundedCell: UIView {
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        self.layer.cornerRadius = bounds.height / 4
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        self.layer.cornerRadius = bounds.height / 4
    }
}
