//
//  ContractTableViewCell.swift
//  NewTestApp
//
//  Created by macos on 7/1/18.
//  Copyright © 2018 macos. All rights reserved.
//

import UIKit

class LoadTableViewCell: UITableViewCell {
  
    @IBOutlet weak var activityIndicator: UIActivityIndicatorView!
    @IBOutlet weak var tryButton: UIButton!
    
    func map(showTryButton: Bool) {
        
        if showTryButton {
            tryButton.isHidden = false
            activityIndicator.stopAnimating()
            activityIndicator.isHidden = true
        } else {
            tryButton.isHidden = true
            activityIndicator.startAnimating()
            activityIndicator.isHidden = false
        }
    }
    
}

class ContractTableViewCell: UITableViewCell {
    
    private static var sumFormatter: NumberFormatter {
        let formatter = NumberFormatter()
        formatter.numberStyle = .currency
        
        return formatter
    }
    
    // MARK: Constants
    private let kDefaultColor = UIColor.green
    private let kDebtColor = UIColor.red
    
    // MARK: IBOutlets
    @IBOutlet var view: UIView!
    @IBOutlet var contractName: UILabel!
    @IBOutlet var contractSum: UILabel!
    @IBOutlet weak var imgView: UIImageView!
    
    override func prepareForReuse() {
        super.prepareForReuse()
        contractName.text = nil
        contractSum.text = nil
    }
    
    // MARK: Public methods
    func map(model: ContractsData) {
        
        contractName.text = model.name
        
        let formatter = NumberFormatter()
        formatter.numberStyle = .currency
        contractSum.text = formatter.string(from: (model.sum as NSNumber?)!)
        
        if model.type == "CreditCard" {
            imgView.image = UIImage(named: "CreditCard")
        } else {
            imgView.image = UIImage(named: "CreditLoan")
        }
        
        let hasDebt = model.debt != nil
        view.backgroundColor = hasDebt ? kDebtColor : kDefaultColor
    }
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
}
