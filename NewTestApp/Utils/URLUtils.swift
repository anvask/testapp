//
//  URLUtils.swift
//  NewTestApp
//
//  Created by macos on 7/3/18.
//  Copyright © 2018 macos. All rights reserved.
//

import Foundation

extension URL {
    func withQueryItems(_ queryItems: [URLQueryItem]) -> URL? {
        guard var urlComponents = URLComponents(url: self, resolvingAgainstBaseURL: true) else {
            return nil
        }
        urlComponents.queryItems = queryItems
        
        return urlComponents.url
    }
}
