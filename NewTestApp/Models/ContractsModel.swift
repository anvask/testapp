//
//  ContractsModel.swift
//  NewTestApp
//
//  Created by macos on 7/1/18.
//  Copyright © 2018 macos. All rights reserved.
//

import Foundation

class ContractsData: Codable {
    var contractNumber: String = ""
    var debt: String?
    var name: String = ""
    var signDate: String = ""
    var status: String = ""
    var sum: Double = 0
    var type: String = ""
}
