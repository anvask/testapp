//
//  AutentificationClass.swift
//  NewTestApp
//
//  Created by macos on 6/24/18.
//  Copyright © 2018 macos. All rights reserved.
//

import Foundation

protocol IAuthorizationManager {
    
    func authorization(login: String, password: String, completionHandler: ((Bool, Error?) -> Void)?)
}

class AuthManager: IAuthorizationManager {
    
    static let shared = AuthManager()
    
    private var webService: IWebService = WebService.shared
    
    // MARK: Public methods
    func authorization(login: String, password: String, completionHandler: ((Bool, Error?) -> Void)?) {
        webService.authorization(login: login, password: password) { (token, error) in
            if let error = error {
                completionHandler?(false, error)
                return
            }
            self.webService.token = token
            
            let success = token?.isEmpty == false
            if success {
                self.setDefaults(key: "token", value: token! )
                completionHandler?(success, nil)
            } else {
                completionHandler?(false, WebServiceError.noToken)
            }
            
        }
    }
    
    // MARK: Private methods
    func setDefaults(key: String, value: String) {
        let defaults = UserDefaults.standard
        defaults.set(value, forKey: key)
    }
}
