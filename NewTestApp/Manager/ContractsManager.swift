//
//  ContractsManager.swift
//  NewTestApp
//
//  Created by macos on 7/1/18.
//  Copyright © 2018 macos. All rights reserved.
//

import Foundation

protocol IContractsManager {
    
    func getContracts(completionHandler: (([ContractsData]?, Error?) -> Void)?)
}

class ContractsManager: IContractsManager {
    
    static let shared = ContractsManager()
    
    private var webService: IWebService = WebService.shared
    
    func getContracts(completionHandler: (([ContractsData]?, Error?) -> Void)?) {
        
        webService.getContractList { (contracts, error) in
            if let error = error {
                completionHandler?(nil, error)
                return
            }
            
            completionHandler?(contracts, nil)
        }
    }
}
