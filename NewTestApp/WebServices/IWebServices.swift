//
//  IWebServices.swift
//  NewTestApp
//
//  Created by macos on 7/1/18.
//  Copyright © 2018 macos. All rights reserved.
//

import Foundation


typealias WebServiceAuthHandler = (String?, Error?) -> Void
typealias WebServiceGetContractsHandler = ([ContractsData]?, Error?) -> Void

protocol IWebService {
    var token: String? { get set }
    
    func authorization(login: String, password: String, completionHandler: WebServiceAuthHandler?)
    func getContractList(completionHandler: WebServiceGetContractsHandler?)
}
