//
//  WebService.swift
//  NewTestApp
//
//  Created by macos on 6/24/18.
//  Copyright © 2018 macos. All rights reserved.
//

import Foundation

enum WebServiceError: Error {
    case invalidURL
    case invalidResponse(Any?)
    case failedStatusCode(Int)
    case noToken
}

enum ServiceError: Error {
    
    case network(WebServiceError)
    case serialization(Error)
    case database(Error)
}

class WebService: IWebService {
    
    static let shared = WebService(host: WebServiceEndpoints.shared.WebServiceURL)
    
    // MARK: Constants
    private let kSuccessCode = 200
    private let kForbiddenCode = 403
    
    // MARK: private properties
    private var headers = [String: String]()
    private var sessionConfiguration: URLSessionConfiguration
    private(set) var host: URL
    
    var token: String? {
        didSet {
            headers["Authorization"] = token
            sessionConfiguration.httpAdditionalHeaders = headers
        }
    }
    
    init(host: URL) {
        self.host = host
        sessionConfiguration = URLSessionConfiguration.default
    }
    
    func authorization(login: String, password: String, completionHandler: WebServiceAuthHandler?) {
        let queryItems = [URLQueryItem(name: "login", value: login), URLQueryItem(name: "password", value: password)]
        
        guard let url = URL(string: "/api/auth", relativeTo: host)?.withQueryItems(queryItems)
            else {
                OperationQueue.main.addOperation {
                    completionHandler?(nil, WebServiceError.invalidURL)
                }
                return
        }
        let session = URLSession(configuration: sessionConfiguration)
        
        let task = session.dataTask(with: url) { (data: Data?, response: URLResponse?, error: Error?) in
            if let error = error {
                OperationQueue.main.addOperation {
                    completionHandler?(nil, error)
                }
                return
            }
            
            if let response = response as? HTTPURLResponse {
                if response.statusCode != self.kSuccessCode {
                    OperationQueue.main.addOperation {
                        completionHandler?(nil, WebServiceError.failedStatusCode(response.statusCode))
                    }
                    return
                }
            }
            
            guard let data = data else {
                OperationQueue.main.addOperation {
                    completionHandler?(nil, WebServiceError.noToken)
                }
                return
            }
            
            do {
                let json = try JSONSerialization.jsonObject(with: data, options: .allowFragments)
                
                if let token = (json as? [String: Any])?["token"] as? String {
                    OperationQueue.main.addOperation {
                        completionHandler?(token, nil)
                    }
                }
                else {
                    OperationQueue.main.addOperation {
                        completionHandler?(nil, WebServiceError.invalidResponse(json))
                    }
                }
            }
            catch {
                OperationQueue.main.addOperation {
                    completionHandler?(nil, error)
                }
            }
            
        }
        task.resume()
    }
    
    func getContractList(completionHandler: WebServiceGetContractsHandler?) {
        guard let url = URL(string: "/api/contract", relativeTo: host) else {
            OperationQueue.main.addOperation {
                completionHandler?(nil, WebServiceError.invalidURL)
            }
            return
        }
        let session = URLSession(configuration: sessionConfiguration)
        let task = session.dataTask(with: url) { (data: Data?, response: URLResponse?, error: Error?) in
            if let error = error {
                OperationQueue.main.addOperation {
                    completionHandler?(nil, error)
                }
                return
            }
            if let httpResponse = response as? HTTPURLResponse {
                if httpResponse.statusCode != self.kSuccessCode {
                    OperationQueue.main.addOperation {
                        completionHandler?(nil, WebServiceError.failedStatusCode(httpResponse.statusCode))
                    }
                    return
                }
            }
            guard let data = data else {
                OperationQueue.main.addOperation {
                    completionHandler?(nil, ServiceError.database(error!))
                }
                return
            }
            do {
                let decoder = JSONDecoder()
                let contracts = try decoder.decode([ContractsData].self, from: data)
                OperationQueue.main.addOperation {
                    completionHandler?(contracts, nil)
                }              
                
            }
            catch {
                OperationQueue.main.addOperation {
                    completionHandler?(nil, ServiceError.serialization(error))
                }
            }
        }
        task.resume()
    }
 
}
